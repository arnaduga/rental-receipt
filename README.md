# Introduction (FRA)

*English version: please scroll down*

Si vous avez un logement et que vous le louez, vous savez que vous êtes dans l'obligation légale de fournir une **quittance**.

Ce n'est pas très complexe à produire en utiisant Microsoft Word ou Google Document, cependant, c'est toujours plus amusant quand vous pouvez développer un petit truc, un script, une application qui vous évite de répéter les mêmes actions, au risque de se tromper dans un copier/coller. Bref, soyons fainéant!

C'est exactement l'objet de cette application : un formulaire à remplir (ou déjà pré-rempli si vous l'avez déjà utilisé, grâce au *LocalStorage*) et ... Voilà ! Vous avez maintenant votre PDF, prêt à être signé et envoyé !

## Installation

### Méthode 1 : Docker, depuis le DockerHub

Une image de cette applcation est disponible sur le [Docker Hub](https://hub.docker.com). Donc, pour l'utiliser sur votre propre machine, si vous avez Docker installé, vous pouvez saisir:

`docker run -p 88:5000 arnaduga/rental-receipt:latest`

Et ensuite, ouvrir votre navigateur à l'adresse [http://localhost:88/](http://locahost:88).


Bien sûr, vous pouvez changer le PORT si nécessaire : pour cela, remplacez `88` par ce que vous voulez !


### Méthode 2 : Docker, en construisant sa propre image

Si vous souhaiterz *bidouiller* ou si vous ne souhaitez pas utiliser l'image présente sur le Docker hub (quelqu'en soit la raison), vous pouvez aussi reconstruire l'image par vous-même.

1. Récupérez les sources : `git clone https://gitlab.com/arnaduga/rental-receipt.git`
2. Changez de répertoire : `cd rental-receipt`
3. Construisez l'image : `docker build -t <your-username>/rental-receipt .`, en remplaçant `<your-username>` par votre nom d'utilisateur Docker (bopnne pratique, mais non obligatoire)
4. Démarrez le conteneur : `docker run -p 88:5000 <your-username>/rental-receipt:latest`

Et ensuite, ouvrir votre navigateur à l'adresse [http://localhost:88/](http://locahost:88).

### Méthode 3 : Exécution par NodeJS

Pour cela, vous devez avoir [NodeJS](https://nodejs.org) installé sur votre poste (et idéalement [NVM](https://github.com/nvm-sh/nvm), vous verrez, ça vous aidera !).

Ce projet a été fait en utilisant la version `12.18` mais pourrait fonctionner avec d'autres versions, à vous de voir.

Pour vérifier la version de votre NodeJS, tapez `node --version` 

Si tout est bon, c'est parti pour l'installation locale :
1. Récupérez les sources : `git clone https://gitlab.com/arnaduga/rental-receipt.git`
2. Changez de répertoire : `cd rental-receipt/core`
3. Installez les dépendances : `npm install`
4. Démarrez l'application en saisissant `npm start`

Et ensuite, ouvrir votre navigateur à l'adresse [http://localhost:88/](http://locahost:88).


## Note importante à propos du formulaire pré-rempli

> Si vous avez déjà utilisé l'application, le formulaire sera pré-rempli. 
>
> **Rassurez-vous** : aucune donnée n'est envoyée ! 
> 
> Tout a été sauvegardé dans votre navigateur et son *LocalStorage*, un équivalent des cookies, en quelques sortes.
>
> Plus d'informations sur la page [Wikipedia - LocalStorage](https://fr.wikipedia.org/wiki/Stockage_web_local) 





# Introduction (ENG)
If you have a house/appartment you are currently renting, you should have to provide a *rental receipt*. In France, it is mandatory and it is called **quittance**.

It is not complex to created a rental receipt by hand, using Microsoft Word or Google Document, however, it is always more fun when you can develop some scripts to help you repeating over and over the same actions: let's be lazzy!

This is exactly the purpose of this application: a form to filled (or pre-filled if you already use it, thanks to *LocalStorage*) and ... Done! You have then your PDF file, to be signed and sent!

## Installation

### Method 1: Docker, from DockerHub

An image of this application is available on [Docker Hub](https://hub.docker.com). To use it on your own machine, with Docker pre-installed, type:

`docker run -p 88:5000 arnaduga/rental-receipt:latest`

And open your browser to the address [http://localhost:88/](http://locahost:88)

You can adjust the PORT if required : replace `88` by the expected value.


### Method 2: Docker, your own imaged

If you need to hack the applciation or if you don't want to use the Docker Hub image gfor any reason, you can build your own image locally

1. Get a local copy of sources: `git clone https://gitlab.com/arnaduga/rental-receipt.git`
2. Change your current directody: `cd rental-receipt`), 
3. Build the image: `docker build -t <your-username>/rental-receipt .`, where `<your-username>` is your Docker username
4. Start the container: `docker run -p 88:5000 <your-username>/rental-receipt:latest`

And open your browser to the address [http://localhost:88/](http://locahost:88)

### Method 3:  NodeJS exefcution

For this method, you must have a [NodeJS](https://nodejs.org) installed on your computer (and even [NVM](https://github.com/nvm-sh/nvm), you'll see, it is useful!).

This project was made using the version `12.18` but may work with other version, up to you to check.

To check your NodeJS version, type `node --version`

If everything is fine, let's go for the local installation:
1. Get the sources: `git clone https://gitlab.com/arnaduga/rental-receipt.git`
2. Change the current directory: `cd rental-receipt/core`
3. Install dependencies: `npm install`
4. Start the application: `npm start`

And open your browser to the address [http://localhost:88/](http://locahost:88)


## Important note about pre-filled form

> If you already used the application, the form may be pre-filled.
>
> **Don't worry** : no data have been previously sent! 
> 
> Everything is stored locally in your browser using *LocalStorage*, a kind of cookie principles.
>
> More information on [Wikipedia - LocalStorage](https://en.wikipedia.org/wiki/Web_storage) page

