# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


### v1.0.4 // Not released
### Added
- `publish.sh` script, to be tested...

## v1.0.3 // March, 30th 2021
### Changed
- CICD - Usage of another Docker image (Alpine, not Ubuntu)

## v1.0.2 // March, 29th 2021
### Added
- Generating overlay, but not used for the moment...
- MAP view
- Style selector and submit this information to server
- CICD - Added a way to read version from `package.json`

### Changed
- Server to accept template parameter
- Minor update to try to improve CICD
- CICD - Finally OK

## [1.0.1]
### Changed
- PDF margins

## [1.0.0]
### Added
- Creation of the whole system
- Added `Dockerfile`file
- Added Procfile for Heroku (maybe removed soon)
- Added the README doc... finalement ! \o/
- Added the `gitlab-ci.yml` file
- Added the Preview button

### Changed
- Updated the Puppeteer calls, due to Docker environment
- Move application into `/core` folder, for cleaner folder
- Updated Dockerfile accordingly
- Updated README accordingly

### Fixed
- Minor PDF export presentation issues