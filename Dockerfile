FROM node:20-alpine
LABEL name "Quittance-as-a-Service"


# The tricky is: when in DOCKER, the used Chromium is not the one in PUPPETEER, but the one we need to install
RUN apk update && apk add --no-cache nmap && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
    apk update && \
    apk add --no-cache \
    chromium \
    harfbuzz \
    "freetype>2.8" \
    ttf-freefont \
    nss

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
ENV CONTEXT DOCKER 

# It's a good idea to use dumb-init to help prevent zombie chrome processes.
ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 /usr/local/bin/dumb-init
RUN chmod +x /usr/local/bin/dumb-init

RUN adduser -D rentaluser
USER rentaluser

# Install the application itself
WORKDIR /app
COPY ./core/package.json .
RUN npm install
COPY ./core .

EXPOSE 5000
ENTRYPOINT ["dumb-init", "--"]
CMD ["npm", "run", "start"]