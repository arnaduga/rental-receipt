#!/bin/sh

# TO BE TESTED WITH NEXT VERSION PUBLISH

version=$(cat core/package.json | jq .version -r)

echo ""
echo "This script is PUBLISHING the version $version of the application."

if [ -z $1 ];
then
    echo ""
    echo "... however, you did not provide any comment." 
    echo "Please add one with './publish.sh \"My wonderful comment\"'"
    exit
fi;

echo ""
echo "Commands typed: \"git tag -a \"$version\" -m \"$1\" && echo git push origin $version\""
echo ""