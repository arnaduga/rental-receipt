const express = require("express");

const mustacheExpress = require("mustache-express");

const puppeteer = require("puppeteer");
var writtenNumber = require("written-number");
writtenNumber.defaults.lang = "fr";

const fs = require("fs"); //to multi-template capability

const PORT = process.env.PORT || 5000;
const HOST = "0.0.0.0";

const app = express();

var moment = require("moment");
moment.locale("fr");

// Config the RENDER engine
app.engine("html", mustacheExpress());
app.set("view engine", "html");
app.use(express.json());
app.use(express.static(__dirname + "/public"));

// Ultra basic logger
function logger(text, level = 0) {
  // Level = 0 (log), 1 (warning), 2 (error), 3 (critical)
  var levelString = ["LOG ", "WARN", "ERR ", "CRIT"];
  console.log(`[${moment().format("YYYY-MM-DDTHH:mm:SS.SSS")}][${levelString[level]}] - ${text}`);
}

function formatAmount(val) {
  return (
    writtenNumber(Math.floor(val)) +
    " euros" +
    (Math.round((val - Math.floor(val)) * 100, 0) > 0
      ? " et " + writtenNumber(Math.round((val - Math.floor(val)) * 100, 0)) + " centimes"
      : "")
  );
}

function prepareTemplate(req) {
  const BODY_TEMPLATE = {
    date: moment().format("dddd DD MMMM YYYY"),
    period: {
      start_date: moment().subtract(1, "months").startOf("month").format("DD MMMM YYYY"),
      end_date: moment().subtract(1, "months").endOf("month").format("DD MMMM YYYY"),
    },
    owner: {
      first_name: "Roberta",
      last_name: "Peppi",
      address: {
        street_1: "12 rue de nulle part",
        street_2: "",
        postal_code: "12345",
        city: "Trouville",
      },
    },
    renter: {
      first_name: "Sandro",
      last_name: "Dipepa",
    },
    house: {
      address: {
        street_1: "124 rue de quelque part",
        street_2: "",
        postal_code: "54321",
        city: "CentreMonde",
      },
    },
    fees: {
      basis: 800,
      extra: 76.54,
    },
  };

  const safeBody = {
    ...BODY_TEMPLATE,
    ...req,
  };

  var result = {
    date: safeBody.date,
    period: {
      start_date: safeBody.period.start_date,
      end_date: safeBody.period.end_date,
    },
    owner: {
      first_name: safeBody.owner.first_name,
      last_name: safeBody.owner.last_name,
      address: {
        street_1: safeBody.owner.address.street_1,
        street_2: safeBody.owner.address.street_2,
        postal_code: safeBody.owner.address.postal_code,
        city: safeBody.owner.address.city,
      },
    },
    renter: {
      first_name: safeBody.renter.first_name,
      last_name: safeBody.renter.last_name,
    },
    house: {
      address: {
        street_1: safeBody.house.address.street_1,
        street_2: safeBody.house.address.street_2,
        postal_code: safeBody.house.address.postal_code,
        city: safeBody.house.address.city,
      },
    },
    fees: {
      total: {
        number: (parseFloat(safeBody.fees.basis) + parseFloat(safeBody.fees.extra)).toFixed(2),
        letters: formatAmount(parseFloat(safeBody.fees.basis) + parseFloat(safeBody.fees.extra)),
      },
      basis: {
        number: parseFloat(safeBody.fees.basis).toFixed(2),
        letters: formatAmount(parseFloat(safeBody.fees.basis)),
      },
      extra: {
        number: parseFloat(safeBody.fees.extra).toFixed(2),
        letters: formatAmount(parseFloat(safeBody.fees.extra)),
      },
    },
  };

  return result;
}

/* Define and apply a LOGGER (very basic... not Winston) */
var myLogger = function (req, res, next) {
  logger(`API called: ${req.method} on ${req.path}`);
  next();
};
app.use(myLogger);

app.get("/export/html", (req, res) => {
  //Multiple template management
  logger(`Modèle demandé : ${req.query.template}`);
  let templateToUse = req.query.template || "template_1";
  templateToUse = `${templateToUse}.html`;

  //Test if file does exist
  if (!fs.existsSync(`views/${templateToUse}`)) {
    logger(`Nope... Le modèle ${templateToUse} n'existe pas ... `);
    templateToUse = "template_1.html";
  } else {
    logger(`Yeah, le fichier de modèle ${templateToUse} existe bien !`);
  }
  logger(`Modèle sélectionné : ${templateToUse}`);

  try {
    var myData = JSON.parse(Buffer.from(req.query.data, "base64").toString("utf-8"));
    var prepared = prepareTemplate(myData);
    res.render(templateToUse, prepared);
  } catch (e) {
    logger("Error while parsing the queryString", 2);
    res.render("error_1.html", {
      error_message: "A mon avis, le champ data est manquant ou corrompu !",
    });
  }
});

app.get("/export/pdf", (req, res) => {
  (async () => {
    try {
      logger("Parsing the queryString...");
      var myData = JSON.parse(Buffer.from(req.query.data, "base64").toString("utf-8"));
    } catch (e) {
      logger("Error while parsing the queryString", 2);
      res.render("error_1.html", {
        error_message: "A mon avis, le champ data est manquant ou corrompu !",
      });
      return;
    }

    var page = null;
    var puppeteerConfig = {};

    // Trick to be able to run in DOCKER environment... Thanx Puppeteer... :-(
    if (process.env.CONTEXT == "DOCKER") {
      logger("DOCKER mode identified, setting up Puppeteer differently");
      puppeteerConfig.executablePath = "/usr/bin/chromium-browser";
      puppeteerConfig.args = ["--no-sandbox", "--disable-setuid-sandbox", "--disable-gpu"];
      puppeteerConfig.headless = true;
    }

    try {
      logger("Launching headless chromium (cross-fingers)...");
      const browser = await puppeteer.launch(puppeteerConfig);
      page = await browser.newPage();
    } catch (e) {
      logger(`Error when loading Puppeteer.\n ${e}`, 3);
      res.render("error_1.html", {
        error_message: ":( Gros problème avec Pupeteer... encore",
      });
      return;
    }

    logger("Preparing pazge to load...");
    var pageToLoad = (req.protocol + "://" + req.get("host") + req.originalUrl).replace("/pdf", "/html");

    if (process.env.CONTEXT == "DOCKER") {
      pageToLoad = `http://localhost:${PORT}${req.originalUrl.replace("/pdf", "/html")}`;
      logger(`In DOCKER context, updating the URL to localhost:${PORT}`);
    }

    logger(`Loading /html page...`);
    await page.goto(pageToLoad, { waitUntil: "networkidle2" });

    logger("Converting into PDF...");
    const buffer = await page.pdf({
      format: "A4",
      printBackground: true,
      displayHeaderFooter: false,
      margin: {
        top: "1.5 cm",
        bottom: "1.5 cm",
        left: "1 cm",
        right: "1 cm",
      },
    });
    res.type("application/pdf");

    logger("Presenting file for download...");
    var filename = moment().subtract(1, "months").format("YYYY.MM") + " - Quittance de loyer.pdf";
    res.set({ "Content-disposition": "attachment; filename=" + filename });
    res.send(buffer);
    //browser.close();
  })();
});

process.on("SIGINT", function () {
  logger("Caught interrupt signal. Gracefully stop the application. Bye!");
  process.exit(0);
});

module.exports = { app, PORT, HOST, logger };
