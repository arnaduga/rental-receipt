const application = require("./app");

var server = application.app.listen(application.PORT, () => {
  application.logger(
    `********************************************************************************`
  );
  application.logger(`SERVER STARTED on PORT ${application.PORT}`);
  application.logger(
    `********************************************************************************`
  );
  application.logger(
    `To access the application, launch http://127.0.0.1:${application.PORT}`
  );
  application.logger(
    `If you run this via Docker, do not forget to MAP the port by adding "-p 8080:${application.PORT}" for instance`
  );
  application.logger(
    `--------------------------------------------------------------------------------`
  );
});

module.exports = server;
