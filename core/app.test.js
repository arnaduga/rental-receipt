const app = require("./app.js");
const request = require("supertest");

const log = console.log;
console.log = () => {};

const VALID_QUERYSTRING =
  "eyJkYXRlIjoibHVuZGkgMDYganVpbGxldCAyMDIwIiwicGVyaW9kIjp7InN0YXJ0X2RhdGUiOiIwMSBqdWluIDIwMjAiLCJlbmRfZGF0ZSI6IjMwIGp1aW4gMjAyMCJ9LCJvd25lciI6eyJmaXJzdF9uYW1lIjoiUm9iZXJ0IiwibGFzdF9uYW1lIjoiTWFjcm9ub3MiLCJhZGRyZXNzIjp7InN0cmVldF8xIjoiMTIgcnVlIGRlIG51bGxlIHBhcnQiLCJwb3N0YWxfY29kZSI6IjEyMzQ1IiwiY2l0eSI6IlRyb3V2aWxsZSJ9fSwicmVudGVyIjp7ImZpcnN0X25hbWUiOiJMYcOvbGEiLCJsYXN0X25hbWUiOiJTeW1wYXRpcXVlIn0sImhvdXNlIjp7ImFkZHJlc3MiOnsic3RyZWV0XzEiOiIxMjQgYXZlbnVlIEpkZSBsYSBSw6lwdWJsaXF1ZSIsInBvc3RhbF9jb2RlIjoiMjM0NTYiLCJjaXR5IjoiS2Vsa2VwYXJ0In19LCJmZWVzIjp7ImJhc2lzIjoiMTIzNC41MCIsImV4dHJhIjoiMTIzIn19";
const INVALID_QUERYSTRING =
  "eyJkYXRlIjoibHVuZGkgMDYganVpbGxldCAyMDIwIixwZXJpb2QiOnsic3RhcnRfZGF0ZSI6IjAxIGp1aW4gMjAyMCIsImVuZF9kYXRlIjoiMzAganVpbiAyMDIwIn0sIm93bmVyIjp7ImZpcnN0X25hbWUiOiJSb2JlcnQiLCJsYXN0X25hbWUiOiJNYWNyb25vcyIsImFkZHJlc3MiOnsic3RyZWV0XzEiOiIxMiBydWUgZGUgbnVsbGUgcGFydCIsInBvc3RhbF9jb2RlIjoiMTIzNDUiLCJjaXR5IjoiVHJvdXZpbGxlIn19LCJyZW50ZXIiOnsiZmlyc3RfbmFtZSI6Ikxhw69sYSIsImxhc3RfbmFtZSI6IlN5bXBhdGlxdWUifSwiaG91c2UiOnsiYWRkcmVzcyI6eyJzdHJlZXRfMSI6IjEyNCBhdmVudWUgSmRlIGxhIFLDqXB1YmxpcXVlIiwicG9zdGFsX2NvZGUiOiIyMzQ1NiIsImNpdHkiOiJLZWxrZXBhcnQifX0sImZlZXMiOnsiYmFzaXMiOiIxMjM0LjUwIiwiZXh0cmEiOiIxMjMifX0=";

const VALID_AMOUNT = /1357\.50/;

describe("Test the / path", () => {
  test("It should response the GET method", (done) => {
    request(app.app)
      .get("/")
      .then((response) => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });

  test("Response should get the appropriate title", (done) => {
    request(app.app)
      .get("/")
      .then((response) => {
        expect(response.text).toMatch(/Générateur de quittance/);
        done();
      });
  });

  test("Response should have the SUBMIT button", (done) => {
    request(app.app)
      .get("/")
      .then((response) => {
        expect(response.text).toMatch(/btnGen/);
        done();
      });
  });
}); //End of / tests

describe("Test the /export/html path", () => {
  test("It should send back error if no queryString", (done) => {
    request(app.app)
      .get("/export/html")
      .then((response) => {
        expect(response.text).toMatch(/Erreur/);
        done();
      });
  });

  test("It should send HTML back if valid queryString", (done) => {
    request(app.app)
      .get(`/export/html?data=${VALID_QUERYSTRING}`)
      .then((response) => {
        expect(response.text).toMatch(/id=\"theReceipt\"/);
        done();
      });
  });

  test("It should send backerror if invalid queryString", (done) => {
    request(app.app)
      .get(`/export/html?data=${INVALID_QUERYSTRING}`)
      .then((response) => {
        expect(response.text).toMatch(/Erreur/);
        done();
      });
  });

  test(`It should have the good total amount (${VALID_AMOUNT})`, (done) => {
    request(app.app)
      .get(`/export/html?data=${VALID_QUERYSTRING}`)
      .then((response) => {
        expect(response.text).toMatch(VALID_AMOUNT);
        done();
      });
  });
}); // End of /export/html tests

describe("Test the /export/pdf path", () => {
  test("It should send back error if no queryString", (done) => {
    request(app.app)
      .get("/export/pdf")
      .then((response) => {
        expect(response.text).toMatch(/Erreur/);
        done();
      });
  });

  test("It should send backerror if invalid queryString", (done) => {
    request(app.app)
      .get(`/export/pdf?data=${INVALID_QUERYSTRING}`)
      .then((response) => {
        expect(response.text).toMatch(/Erreur/);
        done();
      });
  });
}); // End of /export/html tests

// console.log(response.headers);
// console.log(response.statusCode);
// console.log(response.text);
// var keys = Object.keys(response);
// console.log(keys);
